FORMAT: 1A

# API Blueprint example

#### run tests like:

`dredd ./apib.md http://127.0.0.1:8000`

Might need to switch to testing database
since it hits the endpoint for real.

#### run mock server like:

`drakov -f apib.md -p 3000`


## Documents list [/document]

This resource represents list of all documents

### List all documents [GET]

+ Response 200 (application/json)

        [
            {
                "guid": "0c42933b-1674-4722-b260-26598d2b742b",
                "text": "alksdjflkasjdf"
            },
            {
                "guid": "381fa249-8120-4690-a6a3-02616672c939",
                "text": "alksdjflkasjdf"
            }
        ]

### Create a new document [POST]

+ Request (application/json)

        {
            "user_guid": 381fa249-8120-4690-a6a3-02616672c939,
            "file_name": "something.jpg",
            "file_data": data
            "networks": ["ethereum", "bitcoin", "artinii"]
        }

+ Response 201 (application/json)

    + Body

        guid of new document created

            {
                "guid": "eff1c48a-727d-4970-a2c7-960710f6c39c",
            }

