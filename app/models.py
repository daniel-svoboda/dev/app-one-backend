from django.db import models

# Create your models here.

class User(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	url = models.CharField(max_length=128, null=True, blank=True)
	username = models.CharField(max_length=128, null=True, blank=True)
	email = models.CharField(max_length=128, null=True, blank=True)

	class Meta:
	    ordering = ('created',)