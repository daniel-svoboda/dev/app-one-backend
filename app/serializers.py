# from django.contrib.auth.models import User, Group
from .models import User
from rest_framework import serializers


class UserSerializer(serializers.Serializer):
	id = serializers.IntegerField(read_only=True)
	url = serializers.CharField(required=False, allow_blank=True, max_length=128)
	username = serializers.CharField(required=False, allow_blank=True, max_length=128)
	email = serializers.CharField(required=False, allow_blank=True, max_length=128)

	def create(self, validated_data):
	    """
	    Create and return a new `Snippet` instance, given the validated data.
	    """
	    return User.objects.create(**validated_data)
