# from django.urls import include, path
# from rest_framework import routers
# from app import views

# router = routers.DefaultRouter(trailing_slash=False)
# router.register(r'users', views.UserViewSet)

# urlpatterns = router.urls

from django.urls import path
from app import views

urlpatterns = [
    path('users/', views.users_view),
    path('users/<int:pk>/', views.users_detail),
]