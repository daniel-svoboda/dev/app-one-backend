FROM python:3.7-alpine

COPY . /app_one_backend/

ENV TERM xterm
ENV LC_ALL=C.UTF-8
ENV PYTHONPATH /app_one_backend
ENV DEBUG False

WORKDIR /app_one_backend

RUN apk --update add --no-cache --virtual .build-deps libffi-dev build-base

RUN apk --update add --no-cache postgresql-dev

RUN pip3 install --no-cache-dir --upgrade pip && pip3 install -r requirements.txt && \
    mkdir -p /var/log/django && touch /var/log/django/error.log && \
    mkdir -p /var/log/gunicorn && touch /var/log/gunicorn/error.log && \
    touch /var/log/gunicorn/access.log

RUN apk del .build-deps

EXPOSE 8080

ENTRYPOINT ["gunicorn", "--bind", ":8080", "--workers", "5", "--log-level", "info", \
    "--log-file", "/var/log/gunicorn/error.log", "--access-logfile", \
    "/var/log/gunicorn/access.log", "app_one_backend.wsgi:application"]


# infinite loop for inspection/testing only:
#CMD exec /bin/sh -c "trap : TERM INT; (while true; do sleep 1000; done) & wait"
