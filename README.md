# Initial setup
- add env. vars to Gitlab CICD required to run pipeline (listed bellow)
- add secrets to K8s cluster that are required in pod
- any commit will triger CICD and push to Prod env.

# Running locally:
## Docker:
- for sake of local development you may either add postgres or run it in DEBUG=True (not recommended >> defaults to SQLite + DEBUG=True is not the best practice)
- create `.env_local_dev` and input variables needed (listed bellow)
- build image + run 

## Minikube
- setup minikube
- `vi minikube_testing/minikube_deployment.yaml` and adjust the `image version` to get correct image
- `minikube_deployment.yaml` will setup simple Postgres DB, you'll need to run *database migration* first
- via kubectl create all necessary secrets (listed bellow)
- run the `minikube_deployment.yaml` + `service/ingress yamls` if desired

### Notes:
- if you want to use your private gitlab registry docker image login with docker:
	- `kubectl create secret docker-registry gitlab-registry-auth --docker-server=registry.gitlab.com --docker-username=testuser --docker-password=gimmepwd --docker-email=gimme@email.com`
	- minikube deployment automatically looks for `gitlab-registry-auth`
- you cannot run original deployment.yaml as it sets up side-car container to proxy to Google cloud managed SQL DB. 



## Testing

### Postman:
- import `postman_collection.json` into Postman and run as resired

### Curl/HTTPie
- add users via API
- `curl -d '{"url":"testurl.com", "username":"testuser", "email":"test@me.com"}' -H "Content-Type: application/json" -X POST https://awesometest.ga/api/v1/app-one-backend/users/`

- view list of users
`curl GET https://awesometest.ga/api/v1/app-one-backend/users/`


### Testing on localhost
`curl -d '{"url":"testurl.com", "username":"testuser", "email":"test@me.com"}' -H "Content-Type: application/json" -X POST http://localhost:8000/users/`
`curl GET http://localhost:8000/users/`


# Gitlab env vars + K8s secrets setup
## Example of .env_local_dev file

`DEBUG='True'
DATABASE_USER_NAME=''
DATABASE_PASSWORD=''
DATABASE_HOST='127.0.0.1'
DATABASE_PORT='5432'
DATABASE_NAME=''
SECRET_KEY=''`

### Generate SECRET_KEY e.g.:
### `python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())'`


## gitlab CICD env. vars.

`TEST_ENV_VARS=REVCVUc9J0ZhbHNlJwpEQVRBQkFTRV9VU0VSX05BTUU9J3VzZXInCkRBVEFCQVNFX1BBU1NXT1JEPSdwd2QnCkRBVEFCQVNFX0hPU1Q9J3Bvc3RncmVzJwpEQVRBQkFTRV9QT1JUPSc1NDMyJwpEQVRBQkFTRV9OQU1FPSd0ZXN0ZGInClNFQ1JFVF9LRVk9JyFlZzQqIXFAKm5sPSkpQCY1YmErKDcjJWxAaHFlN3pzKF5xOCshcnQmKCVkbTVfcnAtJwo=
STAGING_CLUSTER_NAME=gke-default-default-pool-0b2284e5-tv96
STAGING_URL=35.198.160.000
KUBE_CRED_NAME=app-one-admin
STAGING_KUBE_USER=app-one-admin
STAGING_KUBE_PASSWORD=gimmepwd
STAGING_DB_INSTANCE=app-one:europe-west3:app-one-test`

### Notes:
- TEST_ENV_VARS are base64 encoded env vars required for CICD Test stage (on gitlab runner) to setup postgres so tests will work properly


## K8s secrets
`kubectl create secret generic app-one-backend-db \
--from-literal=DATABASE_NAME='app-one-db' \
--from-literal=DATABASE_USER_NAME='app-one-db' \
--from-literal=DATABASE_PASSWORD='gimmepwd' \
--from-literal=DATABASE_HOST='127.0.0.1' \
--from-literal=DATABASE_PORT='5432'`

`kubectl create secret generic app-one-backend-secret-key \
--from-literal=SECRET_KEY='!eg4*!q@*nl=))@&5ba+(7#%l@hqe7zs(^q8+!rt&(%dm5_rp-'`

`kubectl create secret docker-registry gitlab-registry-auth --docker-server=registry.gitlab.com --docker-username=testuser --docker-password=gimmepwd --docker-email=gimme@email.com`
`kubectl create secret generic cloudsql-oauth-credentials --from-file=credentials.json=cred.json`


